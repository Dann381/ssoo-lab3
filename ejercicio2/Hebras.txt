Las hebras (hilos), le permiten a las aplicaciones realizar tareas en paralelo. Desde un proceso se pueden
subdividir las tareas para que estas sean realizadas por las n hebras creadas. El hecho de que las hebras
nazcan de un mismo proceso, esto hace que compartan los recursos, permitiendo que cualquier hebra pue-
da modificarlos. Cuando una hebra modifica un dato en la memoria, las otras hebras acceden a ese dato
modificado inmediatamente.
Lo que es propio de cada hebra es el contador de programa, la pila de ejecución y el estado de la CPU
(incluyendo el valor de los registros). El proceso sigue en ejecución mientras al menos uno de sus hilos de
ejecución siga activo.
