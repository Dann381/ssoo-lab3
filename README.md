# Lab 3 - Sistema Operativo y Redes

Los archivos contenidos en este repositorio consisten en la creación de dos programas básicos que tienen por objetivo contar las líneas, palabras y caracteres que contiene uno o varios archivos de texto. El primer programa está creado de forma secuencial usando clases, mientras que el segundo, además de usar clases, usa hebras para realizar su propósito (se crea una hebra por cada archivo de texto a procesar).

### Compilación

Para poder compilar cuaquiera de los dos programas, es necesario dirigirse a la ruta en la cual se aloja al programa usando el comando "cd". Una vez dentro de la carpeta (ya sea "ejercicio1" o "ejercicio2"), utilizaremos el comando make para compilar el programa, y ya estará listo para ejecutarse.

### Inicio del programa y uso

Para poder iniciar cualquiera de los dos programas, debemos ejecutarlo mediante la terminal de la siguiente forma: "./ejercicio1". A continuación de escribir el nombre del programa, debemos especificar qué archivo de texto queremos procesar, por ejemplo: "./ejercicio2 Texto.txt". Este programa recibe más de un solo parámetro, por lo que podemos agregar más archivos de texto para procesar. Ejemplo: "./ejercicio1 Frankenstein.txt Hebras.txt Texto.txt". El comando anterior contará las líneas, palabras y caracteres de cada archivo de texto especificado.

### Output y tiempo de ejecución

Luego de terminado el programa, la terminal arrojará un total de todas las líneas, palabras y caracteres de cada archivo de texto por separado, y luego realizará un balance de total de todas las líneas, palabras y caracteres contadas (se suman los datos obtenidos de todos los archivos de texto procesados). Por último, se mostrará el tiempo que demoró el programa en ejecutarse. Para el ejercicio1, el tiempo promedio de ejecución es de 0,0013 segundos, mientras que para el ejercicio2 el tiempo de promedio de ejecución es de 0,003 segundos aproximadamente. Se puede observar que el tiempo de ejecución promedio del ejercicio1 (el cual está hecho de forma secuencial) es menor al tiempo de ejecución promedio del ejercicio2 (el cual utiliza hebras para leer cada archivo), lo que evidencia que la forma secuencial es levemente más rápida que la forma en la cuál se usó hebras.

Cada programa fue probado con diferentes archivos de texto, más grandes y más pequeños en cantidad de caracteres, y los resultados fueron similares en cada prueba (siendo el ejercicio1 más rápido que el ejercicio2).

### Construido con

El presente proyecto de programación, se construyó en base al lenguaje de programación C++, en conjunto con el IDE "Visual Studio Code".

* [C++] - Lenguaje de programación utilizado.
* [Visual Studio Code](https://code.visualstudio.com/) - IDE utilizado.

### Autores

* **Daniel Tobar** - Estudiante de Ingeniería Civil en Bioinformática.
