#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;
#include "Lector.h"

Lector::Lector(){}

//Cuenta las lineas de la misma manera que el ejercicio 1
int Lector::contar_lineas(string archivo){
    ifstream file(archivo);
    string texto;
    int lineas = 0;
    while(getline(file,texto)){
        lineas++;
    }
    file.close();
    return lineas;
}

//Cuenta los caracteres de la misma manera que el ejercicio 1
int Lector::contar_caracteres(string archivo){
    ifstream file(archivo);
    string texto;
    int caracteres = 0;
    while(getline(file,texto)){
        int largo = texto.length();
        for(int i = 0; i < largo; i++){
            if(texto[i] != ' '){
                caracteres++;
            }
        }
    }
    file.close();
    return caracteres; 
}

//Cuenta las palabras de la misma manera que el ejercicio 1
int Lector::contar_palabras(string archivo){
    ifstream file(archivo);
    string texto;
    int palabras = 0;
    int espacios = 0;
    int caracteres_por_linea = 0;
    int temp = 0;
    while(getline(file,texto)){
        caracteres_por_linea = 0;
        espacios = 0;
        int largo = texto.length();
        for(int i = 0; i < largo; i++){
            if(texto[i] != ' '){
                caracteres_por_linea++;
            }
            else if(texto[i] == ' ' && texto[i+1] != ' '){
                espacios++;
            }
        }
        if(texto[0] == ' '){
            espacios = espacios - 1;
        }
        if(caracteres_por_linea > 0){
            temp = espacios + 1;
        }
        else if(caracteres_por_linea == 0){
            temp = 0;
        }
        palabras = palabras + temp;
    }
    return palabras;
}

//Se agregó una función aparte para escribir los datos por la terminal de cada archivo
void Lector::balance_texto(datos Datos){
    cout << "Archivo --> " << Datos.nombre << endl;
    cout << "Lineas: " << Datos.lineas << endl;
    cout << "Caracteres: " << Datos.caract << endl;
    cout << "Palabras: " << Datos.palabras << endl;
}
