#include <pthread.h>
#include <string.h>
#include <iostream>
#include <fstream>
#include <ctime>
#include <unistd.h>
using namespace std;
#include "Lector.h"

//Funcion que utilizarán las hebras, recibe la dirección de la estructura como parámetro
void *leer_archivo(void *temp){
    Lector lector;
    //Se crea la variable tipo "datos"
    datos *Datos;
    //el parametro pasado se guarda en Datos
    Datos = (datos *)temp;
    //Se obtienen las lineas
    Datos->lineas = lector.contar_lineas(Datos->nombre);
    //Se obtienen los caracteres
    Datos->caract = lector.contar_caracteres(Datos->nombre);
    //Se obtienen las palabras
    Datos->palabras = lector.contar_palabras(Datos->nombre);
    //Termina la hebra
    pthread_exit(0);
}

int main(int argc, char **argv){
    clock_t start, end;
    //Toma el tiempo inicial
    start = clock(); 
    Lector lector;
    //Se crean tantas hebras como parametros pasados por el usuario (que son la cantidad
    //de archivos a procesar)
    pthread_t threads[argc-1];
    int systemRet = system("clear");
    if (systemRet == -1){
        //The system method failed
    }
    //Se crea una estructura tipo "datos" por cada archivo pasado por el usuario
    datos Datos[argc-1];
    //Contadores de todos los datos obtenidos de todos los archivos
    int total_lineas = 0;
    int total_caracteres = 0;
    int total_palabras = 0;
    //Se agrega el nombre del archivo a cada estructura correspondiente
    for(int i = 1; i < argc; i++){
        Datos[i-1].nombre = argv[i];
    }

    //Se crean las hebras y se especifica la función y el parámetro que usará
    for(int i = 0; i < argc-1; i++){
        pthread_create(&threads[i], NULL, leer_archivo, &Datos[i]);

    }
    //Se especifica que espere hasta que las hebras terminen
    for (int i = 0; i < argc-1; i++) {
        pthread_join(threads[i], NULL);
    }
    //Cuenta el total de datos obtenidos
    for(int i = 0; i < argc-1; i++){
        total_lineas = total_lineas + Datos[i].lineas;
        total_caracteres = total_caracteres + Datos[i].caract;
        total_palabras = total_palabras + Datos[i].palabras;
    }
    //Indica por la terminal el total de lineas, caracteres y palabras de cada archivo
    for(int i = 0; i < argc-1; i++){
        lector.balance_texto(Datos[i]);
        cout << endl;
    }

    //Indica el total de lineas, caracteres y palabras totales
    cout << "Balance total" << endl;
    cout << "Lineas totales: " << total_lineas << endl;
    cout << "Caracteres totales: " << total_caracteres << endl;
    cout << "Palabras totales: " << total_palabras << endl;
    cout << endl;
    //Toma el tiempo final, y calcula los tiempos de ejecución del programa
    end = clock();
    double tiempo = (double(end-start)/CLOCKS_PER_SEC);
    cout << "Tiempo transcurrido: " << tiempo << " segundos" << endl;
    return 0;
}
