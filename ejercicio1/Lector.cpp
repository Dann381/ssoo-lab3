#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;
#include "Lector.h"

Lector::Lector(){}

//Función para contar las líneas
int Lector::contar_lineas(string archivo){
    //Se abre el archivo
    ifstream file(archivo);
    string texto;
    int lineas = 0;
    //Por cada linea aumenta en 1 el contador
    while(getline(file,texto)){
        lineas++;
    }
    //Se imprime la cantidad de lineas del archivo por terminal
    cout << "Lineas: " << lineas << endl;
    file.close();
    //Se devuelven las lineas para sumarlas al total de lineas en el main
    return lineas;
}

//Funcion para contar los caracteres
int Lector::contar_caracteres(string archivo){
    ifstream file(archivo);
    string texto;
    int caracteres = 0;
    while(getline(file,texto)){
        //Por cada linea se obtiene su largo
        int largo = texto.length();
        //Se recorre la linea contando todo lo que hay excepto los espacios
        for(int i = 0; i < largo; i++){
            if(texto[i] != ' '){
                //Mientras no sea espacio, se suma a la variable caracteres
                caracteres++;
            }
        }
    }
    //Se imprime la cantidad de caracteres del archivo
    cout << "Caracteres: " << caracteres << endl;
    file.close();
    //Retorna los caracteres para sumarlo a la cantidad total de caracteres en el main
    return caracteres; 
}

//Funcion para contar las palabras del archivo
int Lector::contar_palabras(string archivo){
    ifstream file(archivo);
    string texto;
    //variables necesarias para poder hacer la distinción entre palabras
    int palabras = 0;
    int espacios = 0;
    int caracteres_por_linea = 0;
    int temp = 0;
    //Se recorre cada linea en función de su largo
    while(getline(file,texto)){
        caracteres_por_linea = 0;
        espacios = 0;
        int largo = texto.length();
        for(int i = 0; i < largo; i++){
            //Se cuentan los caracteres que posee la linea
            if(texto[i] != ' '){
                caracteres_por_linea++;
            }
            //Aquí se cuentan los espacios (si hay muchos espacios juntos, solo se cuenta como uno)
            else if(texto[i] == ' ' && texto[i+1] != ' '){
                espacios++;
            }
        }
        //Si hay un espacio al empezar el texto, se descuenta del total de espacios
        //(ya que utilizamos la cantidad de espacios para calcular el total de palabras por linea)
        if(texto[0] == ' '){
            espacios = espacios - 1;
        }
        //Si existen caracteres en la linea, la cantidad de palabras es igual a la cantidad
        //de líneas mas 1
        if(caracteres_por_linea > 0){
            temp = espacios + 1;
        }
        //Pero si no hay caracteres, da igual la cantidad de espacios que haya, eso significa que
        //no hay palabras en la linea
        else if(caracteres_por_linea == 0){
            temp = 0;
        }
        //Finalizando las validaciones, se suma lo obtenido a la variable palabras
        palabras = palabras + temp;
    }
    //Se imprime el total de palabras del texto
    cout << "Palabras: " << palabras << endl;
    //Retorna la cantidad de palabras para sumarlas al balance total
    return palabras;
}
