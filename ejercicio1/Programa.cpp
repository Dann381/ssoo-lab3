#include <string.h>
#include <iostream>
#include <fstream>
#include <ctime>
using namespace std;
#include "Lector.h"

int main(int argc, char **argv){
    //Toma el tiempo inicial
    clock_t start = clock();
    //Se limpia el terminal
    int systemRet = system("clear");
    if (systemRet == -1){
        //The system method failed
    }
    Lector lector = Lector();
    //Se crean variables para contar los totales
    int total_lineas = 0;
    int total_caracteres = 0;
    int total_palabras = 0;
    //Entra al ciclo que cuenta líneas, palabras y caracteres por cada archivo
    for(int i = 1; i < argc; i++){
        cout << "ARCHIVO >>> " << argv[i] << endl;

        //La función devuelve las lineas del archivo
        int line = lector.contar_lineas(argv[i]);
        total_lineas = total_lineas + line;

        //La función devuelve los caracteres del archivo
        int caracter = lector.contar_caracteres(argv[i]);
        total_caracteres = total_caracteres + caracter;

        //La función devuelve las palabras obtenidas
        int palabras = lector.contar_palabras(argv[i]);
        total_palabras = total_palabras + palabras;

        cout << endl;
    }

    //Teniendo los totales, se imprimen en la terminal como el Balance Total
    cout << "Balance total" << endl;
    cout << "Lineas totales: " << total_lineas << endl;
    cout << "Caracteres totales: " << total_caracteres << endl;
    cout << "Palabras totales: " << total_palabras << endl;
    cout << endl;
    //Se toma el tiempo final
    clock_t end = clock();
    //Se obtiene el tiempo de ejecución restando el tiempo final con el tiempo inicial
    double tiempo = (double(end-start)/CLOCKS_PER_SEC);
    cout << "Tiempo transcurrido: " << tiempo << " segundos" << endl;
    return 0;
}
