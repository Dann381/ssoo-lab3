#ifndef LECTOR_H
#define LECTOR_H

class Lector{
    public:
        Lector();
        int contar_lineas(string archivo);
        int contar_caracteres(string archivo);
        int contar_palabras(string archivo);
};
#endif