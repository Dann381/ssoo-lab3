#ifndef LECTOR_H
#define LECTOR_H
#include <fstream>
#include <iostream>
using namespace std;

//Estructura creada para guardar los datos de cada archivo de texto
typedef struct {
    string nombre; //Nombre del archivo de texto
    int lineas; //Lineas que posee
    int caract; //Caracteres que posee
    int palabras; //Palabras que posee
} datos;
//Se guardan en una estructura para no perder los datos, y para luego poder contar el total
//sumando los datos de cada archivo

class Lector{
    public:
        Lector();
        int contar_lineas(string archivo);
        int contar_caracteres(string archivo);
        int contar_palabras(string archivo);
        void balance_texto(datos Datos);
};
#endif